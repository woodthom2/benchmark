import time

class Benchmark:    
    def __init__(self, description):
        self.description = description

    def __enter__(self):
        print ("Starting timer: ", self.description)
        self.start = time.clock()
        return self

    def __exit__(self, *args):
        self.end = time.clock()
        self.interval = self.end - self.start
        print ("Ended timer: ", self.description, "in", self.interval, "seconds.")
